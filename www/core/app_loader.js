var AppVer = '0.0.1';
var appscripts = [
	'core/jquery-1.12.4.min.js',
	'core/jquery.mobile.custom.min.js',
	'core/ua-parser.min.js',
	'core/application.js',
];

var appscripts_loaded = 0;
var apptype = 'production';
var app_url = 'http://www.cincopa.com/_cms/application/';
var ajax_url = 'http://www.cincopa.com/media-platform/ctv/app-endpoint.aspx';
switch (window.localStorage.getItem('testmode')) {
	case 'localhost':  	
		app_url = './';
		ajax_url = 'http://dev8.cincopa.com/media-platform/ctv/app-endpoint.aspx'; 
	break;
	case 'development':	
		app_url = 'http://dev7.cincopa.com/_cms/application/'; 
		ajax_url = 'http://dev8.cincopa.com/media-platform/ctv/app-endpoint.aspx';
	break;
	default: 			
		app_url = 'http://www.cincopa.com/_cms/application/';
		ajax_url = 'http://www.cincopa.com/media-platform/ctv/app-endpoint.aspx'; 
	break;
}

function add_script_to_head(url){
	var script = document.createElement('script');
	script.type = 'text/javascript';
	
	script.onload = function() {
		appscripts_loaded++;
		if (appscripts_loaded == appscripts.length) {
			App.init();
		} else {
			add_script_to_head(app_url + appscripts[appscripts_loaded]);
		}
	};
	
	script.src = url + '?v=' + AppVer;
	
	document.getElementsByTagName('head')[0].appendChild(script);
}

document.addEventListener("deviceready", function(){add_script_to_head(app_url + appscripts[appscripts_loaded]);}, false);


//Append libasync
document.addEventListener("DOMContentLoaded", function() {
	var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'http://www.cincopa.com/media-platform/runtime/libasync.js?v=' + AppVer;
	document.body.appendChild(script);
});

//Load main css 
var css = document.createElement( "link" )
css.type = "text/css";
css.rel = "stylesheet";
css.href = app_url + 'core/application.css?v=' + AppVer;

document.getElementsByTagName( "head" )[0].appendChild( css );

function a(val) {
	alert(JSON.stringify(val));
}

function random_number(){
	var number = Math.round(Math.random() * 1000000);
	return number;
}
