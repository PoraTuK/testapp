var Application = function() {
	var self = this;

	self.main = false;
	self.orientation = false;
	self.app_orientation = false;
	self.height = false;
	self.width = false;
	self.min = false;
	self.max = false;
	self.event = false;
	self.ratio = window.devicePixelRatio || 1;
	self.qparams = {};
	self.hparams = {};
	self.current_page = false;
	self.page = false;
	self.json = {};
	
	self.parser = new UAParser().getResult();
	self.device = (self.parser.device && self.parser.device.type ? self.parser.device.type : 'mobile' );
	
	//Default url for ajax request to get some data
	self.ajax_url = ajax_url;
	self.app_url = app_url;

	//Initial function for main params
	self.init = function() {
		
		$('body').addClass(self.device).addClass(window.localStorage.getItem('testmode'));
		
		self.get_orientation();
		if (typeof StatusBar == 'object') {
			StatusBar.show();
			StatusBar.overlaysWebView(true);
			alert('red');
			StatusBar.backgroundColorByHexString('#FF0000');
		}
		
		
		//Create own event after rotated device for right
		self.event = document.createEvent("Event");
		self.event.initEvent("rotated", true, true);

		self.calculate_dimensions();
		$(window).bind("resize", function(){ 
			self.calculate_dimensions(); 
		});
		//$('body').css('font-size', (self.max * 0.025) + 'px');
		
		self.main = document.getElementById('main');
			
		self.parse_query_string();
		self.parse_hash_string();
		function hash_change() {
			self.parse_query_string();
			self.parse_hash_string();
			self.change_page();
		}
			
		//Change default page from start.. 
		if (typeof self.hparams.page == 'undefined' || self.hparams.page == '') {
			self.hparams.page = 'channels_list';
		}
		
		window.addEventListener("hashchange", hash_change, true);
		
		//Open menu from left side
		$(App.main).on('swiperight', function(event) {
			if (event.swipestart.coords[0] < App.width * 0.1)
				Menu.show_left_menu();
		});
		
		self.change_page();
	}

	self.loading = function() {
		$('body').append($('<div id="loader"></div>'));
	}

	self.loaded = function() {
		$('#loader').fadeTo(500, 0, function(e) {
			$(this).remove();
		});
	}

	self.calculate_dimensions = function() {
		self.orientation = (window.innerWidth > window.innerHeight) ? 'landscape' : 'portrait';
		$('.video_title').html()
		switch (self.orientation) {
			case 'landscape':
				if (screen.width > screen.height) {
					self.width = Math.max(window.innerWidth, screen.width);
					self.height = Math.max(window.innerHeight, screen.height);
				}
				else {
					self.width = Math.max(window.innerWidth, screen.height);
					self.height = Math.max(window.innerHeight, screen.width);
				}
				break;
			default:
				if (screen.width < screen.height) {
					self.width = Math.max(window.innerWidth, screen.width);
					self.height = Math.max(window.innerHeight, screen.height);
				}
				else {
					self.width = Math.max(window.innerWidth, screen.height);
					self.height = Math.max(window.innerHeight, screen.width);
				}
				break;
		}
		self.width = window.innerWidth;
		self.height = window.innerHeight;
		self.min = Math.min(self.width, self.height);
		self.max = Math.max(self.width, self.height);
		// alert([self.orientation, "or:" + window.orientation,  "\r\n" + self.width,
		 // self.height, "\r\n" + screen.width, screen.height, window.devicePixelRatio,
		  // "\r\n" + window.innerWidth, window.innerHeight]);
		window.dispatchEvent(self.event);
	}

	self.orientation_change = function() {
		var data = arguments;
		var fn = Array.prototype.shift.call(arguments);
		window.addEventListener('rotated', fno = function() {
			fn.apply(window, data);
		});
	}

	self.change_page = function() {
		//If we have same page - just return.. was changed some params
		if (self.current_page === self.hparams.page) return;
		//Unlock orientation if need
		
		//load js and create class if its need;
		if (self.load_class()) return;
		
		
		if (typeof StatusBar == 'object')
			StatusBar.show();
		
		self.current_page = self.hparams.page;
		//Close left menu
		Menu.hide_left_menu();
		
		//Switch to pages
		switch (self.hparams.page) {
			case 'video-play':
				self.lock_orientation('landscape');
			break;
			default:
				//Remove video menu and unlock orientation
				if (Menu.video_menu)
					Menu.video_menu.empty(); 
				Menu.video_menu = false;
				self.lock_orientation();
			break;
		}
		switch (self.hparams.page) {
			case 'video-play':
				self.video_play();
				break;
			case 'channel-show':
				Channel.channel_show(true);
				break;
			case 'add-channel':
				Channels.print_add_channel();
				break;
			case 'channels-list':
			default:
				Channels.print_channels_list();
				break;
		}
	}
	
	self.video_play = function(){
		
					
		if (typeof StatusBar == 'object')
			StatusBar.hide();
		
		$(App.main).empty().append($('<div>', {id: 'video_player'}));
		
		cincopa['widget_max_width'] = '100vw';
		cincopa['widget_max_height'] = '100vh';
		
		cincopa.registerEvent('App.video_player_event',"*");
		cp_load_widget(App.hparams.player + '!' + App.hparams.rid, 'video_player');
	}
	
	self.video_player_event = function(name, data, gallery){
		
		switch (name) {
			case 'runtime.on-html-loaded':
				var title = 'Simpe video';
				if (data && data.MediaJSON && data.MediaJSON.items){
					for (var i in data.MediaJSON.items) {
						if (App.hparams.rid == data.MediaJSON.items[i].rid) {
							title = data.MediaJSON.items[i].title;
						}
					}
				}
				Menu.print_video_header(title);
			break;
			case 'video.play':
				Menu.hide_header();
			break;
			case 'video.pause':
				Menu.show_header();
			break;
			case 'video.ended':
				window.history.back();
			break;
		}
	}
	
	self.print_page_block = function(page_id, data_attributes, title, header_button) {
		
		//Clear all 
		$(App.main).empty();
		//Print menus
		var header = Menu.print_header(title, header_button);
			Menu.print_left_menu();
		
		//Print main thumbinals
		self.page = $('<div>', {id: 'page-' + page_id, class: 'page ' + self.device}).appendTo(App.main);
		if (data_attributes){
			for (var dataname in data_attributes) {
				self.page.attr('data-' + dataname, data_attributes[dataname]);
			}
		}
		
		$(self.page).height(self.height - header.height);
		window.addEventListener('rotated', function() {
			$(App.page).height(App.height - $('#header').height());
		});
		return self.page;
	}
	
	/**
	 *Work with URL parameters
	 */
	
	self.load_class = function(classname) {
		//Load JS for pages if need
		
		if (classname) {
			switch (classname) {
				case 'Channel':
					if ( typeof Channel == 'undefined') {
						self.append_js('channel-show');
						return true;
					}
					break;
				case 'Channels':
					if ( typeof Channels == 'undefined') {
						self.append_js('index');
						return true;
					}
				break;
			}
		} else {
			switch (self.hparams.page) {
				case 'channel-show':
					if ( typeof Channel == 'undefined') {
						self.append_js('channel-show');
						return true;
					}
					break;
				case 'add-channel':
				case 'channels-list':
				case 'play-video':
				default:
					if ( typeof Channels == 'undefined') {
						self.append_js('index');
						return true;
					}
				break;
			}
		}
		return false;
	}
	
	self.scripts_list = [];
	self.append_js = function(name) {
		self.current_page = false;
		
		for (var k in self.scripts_list) {
			if (name == self.scripts_list[k]) {
				return;
			}
		}
		
		var script = document.createElement( "script" )
		script.type = "text/javascript";
		
		script.onload = function() {
			self.change_page();
		};
		
		script.src = self.app_url + 'js/' + name + '.js?v=' + AppVer;
		document.getElementsByTagName( "head" )[0].appendChild( script );
		self.scripts_list.push(name);
	};
	
	self.css_list = [];
	self.append_css = function(name) {
		var css_url = self.css_url + name + '.css?v=' + AppVer;

		for (var k in self.css_list) {
			if (css_url == self.css_list[k]) {
				console.log('CSS loaded:' + css_url);
				return;
			}
		}
		
		var link = document.createElement( "link" )
		link.type = "text/css";
		link.rel = "stylesheet";
		
		
		if (link.readyState) {//IE
			link.onreadystatechange = function() {
				if (link.readyState === "loaded" || link.readyState === "complete") {
					link.onreadystatechange = null;
					self.change_page();
				}
			};
		}
		else {//Others
			link.onload = function() {
				self.change_page();
			};
		}
		
		link.src = css_url;
		document.getElementsByTagName( "head" )[0].appendChild( link );
	};
	
	self.get_orientation = function() {
		self.app_orientation = false;
		if (window.localStorage.getItem('orientation') && window.localStorage.getItem('orientation') != '') {
			self.app_orientation = window.localStorage.getItem('orientation');
		}
		return self.app_orientation;
	}
	
	self.set_orientation = function(orientation) {
		if (orientation && orientation != '') {
			window.localStorage.setItem('orientation', orientation);
			self.app_orientation = orientation;
			return self.app_orientation
		}
		self.app_orientation = false;
		window.localStorage.removeItem('orientation');
		return self.app_orientation
	}
	
	self.lock_orientation = function(orientation){
		var orientation = (orientation !== undefined ?  orientation : self.app_orientation);
		//Unlock orientation after play video
		
		if ( typeof screen.lockOrientation == 'function') {
			if (orientation){
				window.screen.lockOrientation(orientation);
			} else {
				window.screen.unlockOrientation(); 
			}
		}
	}
	
	self.parse_query_string = function(query) {
		var q = query || window.location.search.substring(1);
		var object = (function(a) {
			var qparams = {};
			if (a == "")
				return {};
			for (var i = 0; i < a.length; ++i) {
				var p = a[i].split('=');
				if (p.length != 2)
					continue;
				qparams[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
			}

			return qparams;
		})(q.split("&"));
		if (!query) {
			self.qparams = object
		};
		return object;
	};

	self.parse_hash_string = function(hash) {
		var q = hash || window.location.hash.substring(1);
		var object = (function(a) {
			var hparams = {};
			if (a == "")
				return {};

			for (var i = 0; i < a.length; ++i) {
				var p = a[i].split('=');
				if (p.length != 2)
					continue;
				hparams[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
			}

			return hparams;
		})(q.split("&"));
		
		if (!hash) {
			App.hparams = object
		};

		return object;
	};

	self.parse_params = function(string) {
		var parsed = {};
		if (string) {

			var string = string.split('&');

			for (var i = 0, el; i < string.length; i++) {
				el = string[i].split('=')
				parsed[el[0]] = el[1];
			}
		}

		return parsed;
	}

	self.filter_hash_params = function(params, dparams, exept) {
		var result = {};
		for (var name in params) {
			if ((params[name] && dparams[name] !== params[name]) && $.inArray(name, exept) === -1) {
				result[name] = params[name];
			}
		}
		return result;
	}

	self.get_hash_param = function(name, string) {
		if (!string)
			string = window.location.hash;

		var string = string.replace(/^#/, '');
		if (name) {
			var params = self.parse_params(string);
			if (params[name]) {
				return decodeURIComponent(params[name]);
			}
			else {
				return params[name];
			}
		}
		return self.parse_params(string);
	}

	self.set_hash_param = function(name, value, string) {
		if (!string)
			string = window.location.hash;

		var params = {};
		var string = string.replace(/^#/, '');

		if (string) {
			var params = self.parse_params(string);
		}

		params[name] = value;
		var result = '';
		for (var param in params) {
			result += '&' + param + '=' + encodeURIComponent(params[param]);
		}
		result = '#' + result.replace(/^&/, '');

		return result;
	}
	self.set_hash_params = function(object, hash) {
		if (!hash)
			hash = window.location.hash;

		var params = {};
		var hash = hash.replace(/^#/, '');

		if (hash) {
			var params = self.parse_params(hash);
		}
		$.extend(params, object);

		var result = '';
		for (var param in params) {
			result += '&' + param + '=' + encodeURIComponent(params[param]);
		}
		result = '#' + result.replace(/^&/, '');

		return result;
	}
	self.remove_hash_param = function(name, string) {
		if (!string)
			string = window.location.hash;

		var params = {};
		if (string) {

			var string = string.replace(/^#/, '');
			var params = self.parse_params(string);
		}

		var result = '';

		if (params) {
			for (var param in params) {
				if (name != param) {
					result += '&' + param + '=' + encodeURIComponent(params[param]);
				}
			}
		}

		result = '#' + result.replace(/^&/, '');

		return result;
	}

	self.current_url = function() {
		return window.location.protocol + '//' + window.location.hostname + (window.location.port ? (':' + window.location.port) : '') + window.location.pathname;
	}

	self.current_query = function() {
		return (window.location.search ? window.location.search.substring(1) : false);
	}

	self.current_hash = function() {
		return (window.location.hash ? window.location.hash.substring(1) : false);
	}

	self.change_url = function(url) {
		var url = url || self.current_url() + (self.current_query() ? '?' + self.current_query() : self.current_query()) + (self.current_hash() ? '#' + self.current_hash() : self.current_hash());
		var state = {
			page : url
		};
		var title = $('title').html();
		history.pushState(state, title, url);
		window.history.go(1);
	}
}
var App = new Application();

function Menu_class(){
	var self = this;
	
	self.menu 			= $('<div>', {id: 'menu'});
	self.menu_button 	= false;
	self.menu_list   	= false;
	
	self.video_menu 	= false;
	
	
	/** 
	 * @param {Object|String} title text into the top bar
	 * @param {Object|String} menu_button button to open main left menu on the page
	 * 							false = do not print anything
	 */
	
	self.print_header = function(title, menu_button) {
		// Append current header to the app
		var header = $('<header>', {id: 'header'}).appendTo(App.main);
		//Append menu if button added
		if ( ! menu_button) {
			menu_button = self.print_menu_button();
		} 
		
		header.append(menu_button);
		
		var h1 = $('<h1>', {class: 'header_title'}).appendTo(header);
		if (title){
			h1.append(title);
		} 
		else {
			h1.append(self.get_page_title());
		}
		
		var title = $('<div>', {class: 'menu_title'});
		
		return header;
	}
	
	self.hide_header = function(){
		if (self.menu instanceof jQuery) {
			self.menu.animate({opacity: 0}, 500, 'linear', function(){self.menu.hide();});
			
		}
	}
	
	self.show_header = function(){
		if (self.menu instanceof jQuery) {
			self.menu.show().animate({opacity: 1}, 500, 'linear');
		}
	}
	
	self.print_menu_button = function() {
		var button = $('<span>', {id: 'menu_button', class: 'button'});
			$('<span>', {class: 'fa fa-bars'}).appendTo(button);
		button.click( function() {
			self.show_left_menu();
		});
		
		return button;
	}
	
	self.print_video_header = function(video_name){
		self.video_menu = $('<div>', {id: 'video_header'}).appendTo(document.body);
		var button = $('<span>', {id: 'menu_button', class: 'button'}).appendTo(self.video_menu);
			$('<span>', {class: 'fa fa-arrow-left'}).appendTo(button);
		button.click( function() {
			window.history.back();
		});
	};
	
	self.print_left_menu = function(items, logo, back){
		
		$(self.menu).appendTo(document.body).empty();
		
		var inner = $('<div>', {class: 'menu_inner'}).appendTo(self.menu);
		
		if (logo) {
			$('<div>', {class: 'logo_div'}).appendTo(inner)
				.append($('<a>', {class: 'logo'}).css('background-image' , 'url("' + logo + '")'));
			
		}
		
		//List of items
		var list = $('<ul>', {class: 'menu_list'}).appendTo(inner);
		if (items) {
			
		} else {
			$('<li>', {class: 'menu_item'}).appendTo(list)
			.append ($('<a>', {class:'menu_link', href: '#'})
					.append($('<span>', {class: 'fa fa-home'})).append('Home'));
			
			$('<li>', {class: 'menu_item'}).appendTo(list)
			.append ($('<span>', {class: 'fa fa-user'})).append('Support');
			
			$('<li>', {class: 'menu_item'}).appendTo(list)
			.append ($('<span>', {class: 'fa fa-envelope'})).append('Contact');	
		}
		
		var additional = $('<div>', {class: 'additional'}).appendTo(inner);
		var list = $('<ul>', {class: 'menu_list'}).appendTo(additional);
		$('<li>', {class: 'menu_item'}).appendTo(list)
			.append ($('<span>', {class: 'fa ' + (App.get_orientation() ? 'fa-mobile' :'fa-rotate-left' )}))
			.append(App.get_orientation() ? 'Portrait mode' : 'App rotated')
			.click( function() {
				$(this).empty();
				if (App.get_orientation() == 'portrait') {
					App.set_orientation(false);
					$(this).append($('<span>', {class: 'fa fa-rotate-left'})).append('App rotated');
				} else {
					App.set_orientation('portrait');
					$(this).append($('<span>', {class: 'fa fa-mobile'})).append('Portrait mode');
				}
				App.lock_orientation();
			});
		
		 //navigator.app.exitApp();  // For Exit Application\
		
		return self.menu;
	};
	
	self.show_left_menu = function(){
		$(self.menu).addClass('active');
	};
	
	self.hide_left_menu = function(){
		if (self.menu.hasClass('active')) {
			$(self.menu).removeClass('active');
		}
	};
	
	$(self.menu).on('swipeleft', function(event) {
		if ($(self.menu).hasClass('active')) {
			self.hide_left_menu();
		}
	});
	
	
	self.print_bottom_menu = function(menu_list) {
		var footer = $('<div>', {id: 'left_menu'}).appendTo(self.main);
		var menu = $('<div>', {
			id : 'bottom_menu'
		}).appendTo(footer);
		$('<a>', {
			class : 'menu',
			href : '#' + $.param({
				page : 'channels-list'
			})
		}).html('Channels').appendTo(menu);
		$('<a>', {
			class : 'menu',
			href : '#',
			onclick : "window.open('http://www.cincopa.com', '_system');"
		}).html('Cincopa in browser').appendTo(menu);
		$('<a>', {
			class : 'menu',
			href : '#',
			onclick : "window.open('http://www.google.com', '_system');"
		}).html('Google Browser').appendTo(menu);
		$('<a>', {
			class : 'menu',
			href : 'http://www.google.com'
		}).html('Google').appendTo(menu);
		$('<a>', {
			class : 'menu',
			href : '/'
		}).html('Learn more').appendTo(menu);
		
		return footer;
	}
	
	self.get_page_title = function() {
		var title = 'Cincopa application';
		switch (App.hparams.page){
			case 'channel-show': title = 'List of channels'; break;
			case 'add-channel': title = 'Add channel'; break;
			case 'channels-list': title = 'List yours channels'; break;
			case 'video-play': title = 'Video playback'; break;
		}
		
		return title;
	}
	
	self.print_custom_menu = function(menu_array) {
		
		for (var i = 0, c = menu_array.length; i < c; i++){
			switch (menu_array[i]) {
				
				default :
					
				break;
			}
		}
	}
}

var Menu = new Menu_class();