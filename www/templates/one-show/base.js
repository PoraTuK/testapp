var Gallery = function(json){
	var self = this;
	
	self.ctype 		= 'one-show';
	self.channel	= {};
	self.settings 	= {};
	
	self.subpage 	= false; 
	self.rid 		= false;
	self.fid 		= false;
	
	self.Event 		= false;
	self.div		= false;
	
	
	self.arguments = {
		/* basic */
		master_player: 	{	group: "basic", 
							type:"text", 
							name: "Skin of video player:", 
							default:"A0AArY9KFYaA", 
							editable: true,
							desc:"Insert yours favorite player id"},
		logo: 			{	group: 'basic', 
							type: 'text', 
							name: 'Channel logo',
							editable: true, 
							default: '', 
							desc: 'Set logotype in the header for channel'},
	};
	
	
	self.init = function(json) {
		//Change values
		self.json 		= json;
		self.subpage 	= App.hparams.subpage;
		self.channel 	= Channels.get_channel_by_cid(App.hparams.cid);
		
		if (! self.rid && self.json && self.json.lists) {
			self.rid = Object.keys(self.json.lists)[0];
		}
		
		self.fid 		= false;
		//Change settings for channel
		for(var id in self.arguments) {
			self.settings[id] = self.json.metajson[id] || self.arguments[id].default;
		}
		
		self.change_subpage();
	}
	
	self.change_subpage = function() {
		switch (self.subpage){
			default: 
				self.print_video_list();
			break;
		}
		self.div.addClass(App.device);
	};
	
	self.rid = false;
	self.fid = false;
	
	self.print_video_list = function(){
		
		//Set first video if rid do not set
		if ( self.json && self.json.lists && self.json.lists[self.rid] 
			&& (! self.fid || ! self.json.lists[self.rid].titles[self.fid])) {
			self.fid = Object.keys(self.json.lists[self.rid].titles)[0];
		}
		
		self.div = App.print_page_block(Channel.page, {template: self.channel.ctype, subpage: 'video-list'}, self.channel.cname);
		//Print menu
		$('<img>', {class: 'channel_logo'}).attr('src', self.settings.logo).appendTo($('#header').addClass('with-logo'));
		
		var item = self.json.lists[self.rid].titles[self.fid];
		
		if (item) {
			self.print_video_details(item)
		}
		
		var shows_inner= $('<div>', {class: 'shows_inner'}).appendTo(self.div);
		//Print main thumbinals
		var shows= $('<div>', {id: 'eposides'}).appendTo(shows_inner)
			.on('swipeleft swiperight', function(event){
			
			var shows = $(this);
			
			var show_left = parseInt($(shows).css('left')) || 0;
			var show_width = shows.width();
			var step = 0;
			if (App.orientation == 'portrait'){
				step = App.min; 
			} else {
				while (step + App.min / 3 < App.width) {
					step += App.min / 3; 
				}
			}
			
			if (event.type == "swipeleft" && shows.width() >  (show_left - step) * (-1)) {
				
				if (shows.width() >  (show_left - step * 2) * (-1)) {
					$(shows).animate({'left': (show_left - step) + 'px'}, 500);
				} else {
					$(shows).animate({'left': ((shows.width() - App.width) * -1) + 'px'}, 500);
				}
			}
			
			if (event.type == "swiperight" && 0 > show_left) {
				if (0 >  show_left + step) {
					$(shows).animate({'left': (show_left + step) + 'px'}, 500);
				} else {
					$(shows).animate({'left': '0px'}, 500);
				}
			}
			
			event.stopPropagation();
		});
		//Print bottom video lists
		self.print_channel_videos(shows, self.json.lists[self.rid].titles);
		window.addEventListener("rotated", function() {
			self.print_channel_videos($('#eposides'), self.json.lists[self.rid].titles);
		}, false);
	}
	
	self.print_video_details = function(item) {
		
		if (document.getElementById('video_play')){
			var name = document.getElementById('video_name');
			var description = document.getElementById('video_description');
			var image = document.getElementById('video_image');
			var play = document.getElementById('video_play');
			var favorite = document.getElementById('video_play');
		} else {
			var additional_info = $('<div>', {class: 'video_details'});
				$(self.div).append(additional_info);
				
			var video_texts = $('<div>', {class: 'video_texts'});
			var video_name = $('<p>', {'class':'video_name'}).appendTo(video_texts);
			var favorite = $('<span>', {'id': 'video_favorite', class:"fa fa-heart"}).click(function(){
								$(this).addClass('favorite');
								alert('Added to favorite');
							})
				$(video_name).append(favorite);
			var name = $('<span>', {'id': "video_name"});
				$(video_name).append(name);
				
				$(additional_info).append(video_texts);
				$('<div>', {'class': 'video_about'}).appendTo(video_texts).html('About');
			var description = $('<span>', {'id': 'video_description'}).appendTo(video_texts);
			
			var image = $('<div>', {id: 'video_image'});
			
			var play = $('<a>', {id: 'video_play'}).appendTo(image);
				$('<span>', {class: 'fa fa-play-circle-o '}).appendTo(play);
			$(additional_info).append(image);
		}
		
		$(name).html(item.name);
		$(description).html(item.description || item.thumbnail).on('swipeleft', function() {a('desc');});
		$(image).css('background-image', 'url("' + item.thumbnail + '")');
		
		$(play).attr('href', '#' + $.param({page: 'video-play', rid: self.fid, player: self.settings.master_player}));
	}
	
	self.print_channel_videos = function(object, list) {
		
		$(object).empty();
		var count_lists = 0;
		var width = 0;
		
		for (var fid in list){
			var item = list[fid];
			
			var video_block = $('<div>');
				$(video_block).attr('data-id', fid);
				$(video_block).addClass('video');
				
				//On change video
				$(video_block).click(function(){
					//Add hash param for set video in the list
					self.fid = $(this).attr('data-id'); 
					var item = self.json.lists[self.rid].titles[self.fid];
					self.print_video_details(item);
				});
				
			var video_inner = $('<div>');
				$(video_inner).addClass('video_inner');
				$(video_block).append(video_inner);
				
			var video_link = $('<a>');
				//TODO: Do max size for background image
				$(video_link).css('background-image', 'url("' + item.thumbnail + '")');
				$(video_link).addClass('video_poster');
			$(video_inner).append(video_link);
			$('<p>', {class: 'video_title'}).appendTo(video_inner).html(item.name);	
			
			switch (App.orientation) {
				case 'landscape': 
					$(video_block).width(App.min / 3);
					$(object).append(video_block);
					width += App.min / 3;
				break;
				default: 
					//Create video rows for portrait view
					if (count_lists % 9 == 0) {
						var video_row = $('<div>').addClass('video_row').width(App.width);
						 $(object).append(video_row);
						 width += App.width;
					}
					
					count_lists++;
					$(video_block).width(App.min / 3);
					$(video_row).append(video_block);
				break;
			}
		}
		$(object).width(width);
		if (width < $(object).css('left') * 1 + App.width) {
			$(object).animate({'left' : App.width - width}, 500);
		}
	}
}

 