var Gallery = function(json){
	var self = this;
	
	self.ctype 		= 'video-news';
	self.channel	= {};
	self.settings	= {};
	
	self.subpage 	= false; 
	self.rid 		= false;
	self.fid 		= false;
	
	self.Event 		= false;
	self.statusbar_event = false;
	self.div		= false;
	
	self.arguments = {
		/* basic */
		master_player: 	{	group: "basic", 
							type:"text", 
							name: "Skin of video player:", 
							default:"A0AArY9KFYaA", 
							editable: true,
							desc:"Insert yours favorite player id"},
		logo: 			{	group: 'basic', 
							type: 'text', 
							name: 'Channel logo',
							editable: true, 
							default: '', 
							desc: 'Set logotype in the header for channel'},
		header_bgcolor: {	group: 'basic', 
							type: 'color', 	
							name: 'Header background color',
							editable: true, 
							default: '0086cf', 
							desc: 'Please set header background color'}
	};
	
	self.init = function(json) {
		//Change values
		self.json 		= json;
		self.subpage 	= App.hparams.subpage;
		self.channel 	= Channels.get_channel_by_cid(App.hparams.cid);
		
		if (! self.rid && self.json && self.json.lists) {
			self.rid = Object.keys(self.json.lists)[0];
		}
		
		self.fid 		= false;
		//Change settings for channel
		
		for(var id in self.arguments) {
			self.settings[id] = self.json.metajson[id] || self.arguments[id].default;
		}
		
		self.change_subpage();
	}
	
	
	self.change_subpage = function() {
		if (typeof StatusBar == 'object') {
			StatusBar.show();
		}
		switch (self.subpage){
			case 'show-video':
				self.print_one_video();
			break;
			default: 
				self.print_video_list();
			break;
		}
		self.div.addClass(App.device);
	};
	
	self.print_video_list = function(){
		
		//Print main thumbinals
		self.div = $('<div>', {id: 'page-channel-show'}).appendTo(App.main);
			$(self.div).attr('data-template',  self.channel.ctype);
			$(self.div).attr('data-subpage', 'video-list');
		//Print bottom video lists
		
		self.div = App.print_page_block(Channel.page, {template: self.channel.ctype, subpage: 'video-list'}, self.channel.cname);
		
		var header = $('#header');
		header.css('background', '#' + self.settings.header_bgcolor);
		$('<img>', {class: 'channel_logo'}).attr('src', self.settings.logo).appendTo(header);
		header.addClass('with-logo');
		
		self.print_videos(self.json.lists[self.rid].titles);
	}
	
	self.print_videos = function(list_videos) {
		
		for (var fid in list_videos){
			//Set video url
			var params = {page: Channel.page,
						subpage: 'show-video',
						cid: self.channel.cid,
						fid: fid};
				
			//Create video block with href
			var video_div = $('<a>', {class: 'video_block', id: fid}).appendTo(self.div)
				.attr('href', '#' + $.param(params));
				
			//Thubmnail of video
			var thumb_div = $('<div>', {class: 'video_thumb'}).appendTo(video_div)
				.css('backgroundImage', 'url(' + list_videos[fid].thumbnail +')');
			// Name and description of video
			var desc_div = $('<div>', {class: 'video_text'}).appendTo(video_div);
				$('<h3>', {class: 'video_name'})
					.html(list_videos[fid].name)
					.appendTo(desc_div);
				$('<p>', {class: 'video_desc'})
					.html(list_videos[fid].description)
					.appendTo(desc_div);
		}
	}
	
	/**
	 *Page: Video play 
	 */
	
	self.print_one_video = function() {
		self.fid = App.hparams.fid;
		
		$(App.main).empty();
		$(document.body).animate({scrollTop:0}, 500, 'swing');
		//Print header
		var params = {page: Channel.page,
						cid: self.channel.cid};
		
		var back_button = $('<a>', {class:'fa fa-arrow-left', id: 'menu_button'})
							.attr('href', '#' + $.param(params));
		
		self.div = App.print_page_block(Channel.page, {template: self.channel.ctype, subpage: self.subpage}, self.channel.cname, back_button);
		
		//Print menu
		var header = $('#header');
		header.css('background', '#' + self.settings.header_bgcolor);
		$('<img>', {class: 'channel_logo'}).attr('src', self.settings.logo).appendTo(header);
		header.addClass('with-logo')
		
		self.current_video = $('<div>', {id: 'current_video'}).appendTo(self.div);
		self.print_video_information(self.fid);
			
		var similar_videos = $('<div>', {id: 'similar_videos'}).appendTo(self.div);
			self.print_similar_videos(similar_videos);
	}
	
	self.print_video_information = function(fid) {
		
		if ( ! self.json.lists[self.rid]
			|| ! self.json.lists[self.rid].titles[fid]){
			
			alert("Current video was deleted");
			window.history.back();
			return;
		}
		
		var item = self.json.lists[self.rid].titles[fid];
			item.rid = self.rid;
			item.fid = fid;
		
		if (document.getElementById('video_player')){
			var player = document.getElementById('video_player');
			var name = document.getElementById('video_name');
			var description = document.getElementById('video_desc');
		} else {
			var video_player = $('<div>', {id: 'video_player'}).appendTo(self.current_video);
			var video_texts = $('<div>', {class: 'video_texts'}).appendTo(self.current_video);
			
			var name = $('<h3>', {id: 'video_name'}).appendTo(video_texts);
			var description = $('<p>', {id: 'video_desc'}).appendTo(video_texts);
		}
		
		function template_statusbar(){
			if (App.hparams.page == 'channel-show'
				&& App.hparams.subpage == 'show-video') {
				if (typeof StatusBar == 'object' ) {
					var orientation = 'portrait';
					switch(window.orientation) 
					{
						case -90:
						case 90:
							orientation = 'landscape';
						break; 
						default:
							orientation = 'portrait';
						break; 
					}
					if (screen.orientation && screen.orientation.type){
						if (screen.orientation.type.indexOf('landscape') >= 0 ) {
							orientation = 'landscape';
						}
						else {
							orientation = 'portrait';
						}
					}
					if (orientation == 'landscape') {
						StatusBar.hide();
					}
					else {
						StatusBar.show();
					}
				}
			}
			else {
				window.removeEventListener('orientationchange', template_statusbar, true);
			}
		}
		self.statusbar_event = window.addEventListener('orientationchange', template_statusbar , true);
		template_statusbar();
		
		cincopa["disable_analytics"] = true;
		cincopa['player_watermark'] = false;
		cincopa['next_button'] = false;
		cincopa['playlist_button'] = false;
		cincopa['autostart'] = false;
		cincopa['fullscreen_button'] = true;
		cincopa['play_button'] = false;
		cincopa['prev_button'] = false;
		cincopa.registerEvent('Channel.gallery.video_player_one_video_event',"*");
		cp_load_widget(self.settings.master_player + '!' + fid, 'video_player');
		
		$(name).html(item.name);
		$(description).html(item.description);
	}
	
	self.print_similar_videos = function(similar_videos) {
		var similar_videos = similar_videos;
		
		for (var fid in self.json.lists[self.rid].titles) {
			var item = self.json.lists[self.rid].titles[fid];
			
			var video_div = $('<div>', {class: 'video_div', id: fid}).appendTo(similar_videos);
				if (fid == self.fid) video_div.hide().css('opacity', 0).addClass('current');
				video_div.click(function(){
					if ($(this).hasClass('current')) return;
					
					//Set current file ID
					self.fid = $(this).attr('id');
					self.print_video_information(self.fid);
					//Show previous video on the list
					var current = $(similar_videos).find('.current');
					$(current).show();
					$(current).animate({opacity: 1}, 500);
					$(current).removeClass('current');
					//Hide new selected video 
					var new_similar = this;
					$(new_similar).addClass('current'); 
					$(new_similar).animate({opacity: 0}, 500, 'swing', function(){$(new_similar).hide()});
					//Scroll to top;
					$('body').animate({scrollTop:0}, 500, 'swing');
				});
			var video_image = $('<div>', {class: 'similar_thumb'}).appendTo(video_div);
			var image = $('<img>', {src: item.thumbnail, class: 'similar_image'}).appendTo(video_image);
				
			var video_text = $('<div>', {class: 'similar_texts'}).appendTo(video_div);
				$('<h4>', {class: 'similar_name'}).html(item.name).appendTo(video_text);
				$('<p>', {class: 'similar_desc'}).html(item.description).appendTo(video_text);
		}
	}
	
	self.image_size = function(image){
		var images = image ? [image] : $('#similar_videos similar_thumb');
		$(images).each(function(){
			var div_image = $(this).parent().parent();
				div_image.height(this.height);
		});
	}
	
	self.video_player_one_video_event = function(name, data, gallery){
		
		switch (name) {
			case 'video.ended':
			case 'video.ended':
				window.history.back();
			break;
		}
	}
	
}