var Gallery = function(){
	var self = this;
	
	self.ctype 		= 'banner-show';
	self.channel	= {};
	self.settings	= {};
	
	self.subpage 	= false; 
	self.rid 		= false;
	self.fid 		= false;
	
	self.Event 		= false;
	self.div		= false;
	
	
	self.arguments = {
		/* basic */
		master_player: 	{	group: "basic", 
							type:"text", 
							name: "Skin of video player:", 
							default:"A0AArY9KFYaA", 
							editable: true,
							desc:"Insert yours favorite player id"},
		logo: 			{	group: 'basic', 
							type: 'text', 
							name: 'Channel logo',
							editable: true, 
							default: '', 
							desc: 'Set logotype in the header for channel'},
	};
	
	self.init = function(json) {
		//Change values
		self.json 		= json;
		self.subpage 	= App.hparams.subpage;
		self.channel 	= Channels.get_channel_by_cid(App.hparams.cid);
		
		if (! self.rid && self.json && self.json.lists) {
			self.rid = Object.keys(self.json.lists)[0];
		}
		
		self.fid 		= false;
		
		//Change settings for channel
		for(var id in self.arguments) {
			self.settings[id] = self.json.metajson[id] || self.arguments[id].default;
		}
		
		self.change_subpage();
	}
	
	self.change_subpage = function() {
		
		self.rid = App.hparams.rid;
		
		switch (self.subpage){
			case 'video-list':
				self.print_video_list();
			break;
			default: 
				self.print_galleries_list();
			break;
		}
		self.div.addClass(App.device);
	}
	
	
	/**
	 *Subpage gallery-list 
	 */
	
	self.current_slide = 1;
	self.print_galleries_list = function(){
		
		self.div = App.print_page_block(Channel.page, {template: self.channel.ctype, subpage: 'galleries-list'}, self.channel.cname);
		//Print menu
		var header = $('#header');
		$('<img>', {class: 'channel_logo'}).attr('src', self.settings.logo).appendTo(header);
		header.addClass('with-logo');
		
		//Print main thumbinals
		var slider = $('<div>', {id: 'slider'});
			$(self.div).append(slider);
		
		var slider_items = $('<div>', {id: 'slider_items'}).appendTo(slider);
		var slider_buttons = $('<ul>', {class: 'slider_buttons'}).appendTo(slider);
			$(slider_items);
		if (self.json.banner && self.json.banner.titles && self.json.banner.titles.length) {
			
			$(slider_items).width(App.width * self.json.banner.titles.length);
			//Set change sliders by swiping left and right
			
			$(slider_items).on('swipeleft swiperight', function(e) {
				
				switch(e.type){
					case 'swipeleft': self.current_slide++; break; 
					case 'swiperight': self.current_slide--; break; 
				}
				if (self.current_slide > self.json.banner.titles.length) {self.current_slide = 1;}
				if (self.current_slide <= 0) { self.current_slide = self.json.banner.titles.length;}
				
				$(slider_items).animate({'left': - App.width * (self.current_slide - 1)}, 500);
				
				$('#slider .slider_button').removeClass('current');
				$('#slider .slider_button:eq('+ (self.current_slide - 1) + ')').addClass('current');
				
				e.stopPropagation();
				e.preventDefault();
			});
			
			var c = 0;
			for (var i in self.json.banner.titles){
				c++;
				var item = self.json.banner.titles[i];
				var video_button = $('<li>', {class: 'slider_button'});
				 $(video_button).appendTo(slider_buttons);
				
				var video_block = $('<div>');
					$(video_block).addClass('video');
					$(video_block).width(App.width);
				
				$(slider_items).append(video_block);
				
				var video_link = $('<div>');
					$(video_link).css('background-image', 'url("' +item.poster + '")');
					$(video_link).addClass('video_poster');
				$(video_block).append(video_link);
				
				var size = App.max * 0.05; 
				
				var video_title = $('<span>');
					$(video_title).addClass('video_title');
					$(video_title).html(item.name);
					$(video_title).css('padding-right', ((size * 1.3)) + 'px')
				$(video_block).append(video_title);
				
				var video_play = $('<div>');
					$(video_play).addClass('video_play');
					$(video_play).height(size + 'px').width(size + 'px');
					$(video_play).css('margin-top', (size * (-1)) + 'px');
				var video_play_button = $('<a>');
					$(video_play_button).addClass('play_current_video');
					$(video_play_button).attr('href', '#' + $.param({page: 'video-play', rid: item.reference.split('/')[1], player: self.settings.master_player}));
					$(video_play).append(video_play_button);
				var video_play_icon = $('<span>');
					$(video_play_icon).addClass('fa fa-play');
					$(video_play_icon).css('font-size', ((size- 10) *0.5 ) + 'px');
					$(video_play_icon).css('padding-left', ((size - 10) * 0.5 / 3) + 'px');
					$(video_play_icon).css('line-height', (size - 10) + 'px');
					$(video_play_button).append(video_play_icon);
				
				$(video_block).append(video_play);
				
				//change widht of button by its height
			}
			
			$(slider_items).css('left', - App.width * (self.current_slide - 1));
			$('#slider .slider_button:eq('+ (self.current_slide - 1) + ')').addClass('current');
			self.change_slider_sizes();
			App.orientation_change(self.change_slider_sizes);
		}
		var additional_info = $('<div>', {class: 'additiona_info'});
			$(additional_info).append($('<div>', {class: 'additional_inner', html: '<p>Another playlists of this user</p>'}));
			
			$(self.div).append(additional_info);
		
		var playlist_inner= $('<div>', {class: 'playlist_inner'});
			$(self.div).append(playlist_inner);
		//Print main thumbinals
		var shows= $('<div>', {id: 'playlist'});
			$(playlist_inner).append(shows);
		//Print bottom video lists
		self.print_playlist_list(shows);
		App.orientation_change(self.print_playlist_list, $('#playlist'));
		
	}
	
	self.change_slider_sizes = function(){
		$('#slider').width(App.width);
		$('#slider_items').width(App.width * self.json.banner.titles.length);
		$('#slider_items .video').width(App.width);
		$('#slider_items').css('left', - App.width * (self.current_slide - 1));
		
	}
	
	self.print_playlist_list = function (object) {
		$(object).empty();
		
		var count_lists = 0;
		var width = 0;
		
		if (self.json.lists) {
			for (var key in self.json.lists){
				for (var i = 1; i < 2; i++){
					var item = self.json.lists[key];
					
					var video_block = $('<div>');
						$(video_block).addClass('video');
						$(video_block).attr('id', key);
						$(video_block).width(App.min  / 3);
					
					var video_inner = $('<div>');
						$(video_inner).addClass('video_inner');
						$(video_block).append(video_inner);
					
					var video_link = $('<a>');
						//TODO: Do max size for background image
						var params = $.extend({}, App.hparams);
							params.rid = key;
							params.subpage = 'video-list';
						$(video_link).attr('href', '#' + $.param(params));
						
						$(video_link).css('background-image', 'url("' + item.thumbnail + '")');
						$(video_link).addClass('video_poster');
					$(video_inner).append(video_link);
					var video_title = $('<p>');
						$(video_title).addClass('video_title');
						$(video_title).html(item.name);
					$(video_inner).append(video_title);
					
					switch (App.orientation) {
						case 'landscape': 
							
							$(video_block).width(App.min  / 3);
							//$(video_link).width(App.min / 3);
							$(object).append(video_block);
							width += App.min / 3;
						break;
						default: 
							//Create video rows for portrait view
							if (count_lists % 9 == 0) {
								var video_row = $('<div>');
								 $(video_row).addClass('video_row');
								 $(video_row).width(App.min);
								 $(object).append(video_row);
								 width += App.min;
							}
							count_lists++;
							$(video_row).append(video_block);
						break;
					}
					
				}
			}
		}
		
		$(object).width(width); 
	}
	
	/**
	 *Subpage video-list 
	 */
	
	self.print_video_list = function(){
		
		//Set first video if rid do not set
		if ( self.json && self.json.lists && self.json.lists[self.rid] 
			&& (! self.fid || ! self.json.lists[self.rid].titles[self.fid])) {
			self.fid = Object.keys(self.json.lists[self.rid].titles)[0];
		}
		
		//Print header
		var params = {page: Channel.page,
						cid: self.channel.cid};
		
		var back_button = $('<a>', {class:'fa fa-arrow-left', id: 'menu_button'})
							.attr('href', '#' + $.param(params));
		
		self.div = App.print_page_block(Channel.page, {template: self.channel.ctype, subpage: self.subpage}, self.channel.cname, back_button);
		
		//Print menu
		var header = $('#header');
		$('<img>', {class: 'channel_logo'}).attr('src', self.settings.logo).appendTo(header);
		header.addClass('with-logo')
		
		var item = self.json.lists[self.rid].titles[self.fid];
		
		if (item) {
			self.print_video_details(item)
		}
		
		var shows_inner= $('<div>', {class: 'shows_inner'}).appendTo(self.div);
		//Print main thumbinals
		var shows= $('<div>', {id: 'eposides'}).appendTo(shows_inner);
		shows.on('swipeleft swiperight', function(event, touch){
			
			var show_left = parseInt($(shows).css('left')) || 0;
			var show_width = shows.width();
			var step = 0;
			if (App.orientation == 'portrait'){
				step = App.min; 
			} else {
				while (step < App.width) {
					step += App.min / 3; 
				}
			}
			
			if (event.type == "swipeleft" && shows.width() >  (show_left - step) * (-1)) {
				
				if (shows.width() >  (show_left - step * 2) * (-1)) {
					$(shows).animate({'left': (show_left - step) + 'px'}, 500);
				} else {
					$(shows).animate({'left': ((shows.width() - App.width) * -1) + 'px'}, 500);
				}
			}
			
			if (event.type == "swiperight" && 0 > show_left) {
				if (0 >  show_left + step) {
					$(shows).animate({'left': (show_left + step) + 'px'}, 500);
				} else {
					$(shows).animate({'left': '0px'}, 500);
				}
			}
			
			event.stopPropagation();
		});
		//Print bottom video lists
		self.print_channel_videos(shows, self.json.lists[self.rid].titles);
		window.addEventListener("rotated", function() {
			self.print_channel_videos($('#eposides'), self.json.lists[self.rid].titles);
		}, false);
	}
	
	self.print_video_details = function(item) {
		
		if (document.getElementById('video_play')){
			var name = document.getElementById('video_name');
			var description = document.getElementById('video_description');
			var image = document.getElementById('video_image');
			var play = document.getElementById('video_play');
			var favorite = document.getElementById('video_play');
		} else {
			var additional_info = $('<div>', {class: 'video_details'});
				$(self.div).append(additional_info);
				
			var video_texts = $('<div>', {class: 'video_texts'});
			var video_name = $('<p>', {'class':'video_name'}).appendTo(video_texts);
			var favorite = $('<span>', {'id': 'video_favorite', class:"fa fa-heart"}).click(function(){
								$(this).addClass('favorite');
								alert('Added to favorite');
							})
				$(video_name).append(favorite);
			var name = $('<span>', {'id': "video_name"});
				$(video_name).append(name);
				
				$(additional_info).append(video_texts);
				$('<div>', {'class': 'video_about'}).appendTo(video_texts).html('About');
			var description = $('<span>', {'id': 'video_description'}).appendTo(video_texts);
			
			var image = $('<div>', {id: 'video_image'});
			
			var play = $('<a>', {id: 'video_play'}).appendTo(image);
				$('<span>', {class: 'fa fa-play-circle-o '}).appendTo(play);
			$(additional_info).append(image);
		}
		
		$(name).html(item.name);
		$(description).html(item.description || item.thumbnail);
		$(image).css('background-image', 'url("' + item.thumbnail + '")');
		
		$(play).attr('href', '#' + $.param({page: 'video-play', rid: self.fid, player: self.settings.master_player}));
	}
	
	self.print_channel_videos = function(object, list) {
		
		$(object).html('');
		var count_lists = 0;
		var width = 0;
		
		for (var fid in list){
			var item = list[fid];
			
			var video_block = $('<div>');
				$(video_block).attr('data-id', fid);
				$(video_block).addClass('video');
				
				//On change video
				$(video_block).click(function(){
					//Add hash param for set video in the list
					self.fid = $(this).attr('data-id'); 
					var item = self.json.lists[self.rid].titles[self.fid];
					self.print_video_details(item);
				});
				
			var video_inner = $('<div>');
				$(video_inner).addClass('video_inner');
				$(video_block).append(video_inner);
				
			var video_link = $('<a>');
				//TODO: Do max size for background image
				$(video_link).css('background-image', 'url("' + item.thumbnail + '")');
				$(video_link).addClass('video_poster');
			$(video_inner).append(video_link);
			$('<p>', {class: 'video_title'}).appendTo(video_inner).html(item.name);	
			
			switch (App.orientation) {
				case 'landscape': 
					$(video_block).width(App.height / 3);
					$(object).append(video_block);
					width += App.height / 3;
				break;
				default: 
					//Create video rows for portrait view
					if (count_lists % 9 == 0) {
						var video_row = $('<div>').addClass('video_row').width(App.width);
						 $(object).append(video_row);
						 width += App.width;
					}
					
					count_lists++;
					$(video_block).width(App.min / 3);
					$(video_row).append(video_block);
				break;
			}
		}
		$(object).width(width);
		if (width < $(object).css('left') * 1 + App.width) {
			$(object).animate({'left' : App.width - width}, 500);
		}
	}
}
