var ChannelClass = function ()
{
	var self 			= this;
	self.page			= 'channel-show';
	self.cid 			= false;
	self.load			= true;
	self.data			= {};
	self.json 			= {};
	
	
	self.templates 		= {};
	
	self.gallery		= false;
	
	/**
	 * Load data from server 
	 */
	
	self.load_data = function() {
		if (self.load !== true) return false;
		//Seve caller function for run
		var caller_function = arguments.callee.caller; 
		
		//Change if we need load data again
		self.load = false;
		var channel_type = 'single-show';
		switch (self.data.ctype) {
			case 'one-show':
			case 'video-news': 
				channel_type = 'single-show';
			break;
			case 'banner-show': 
				channel_type = 'multi-shows';
			break;
		}
			
		//Load data
		$.ajax({
			url: App.ajax_url + '?cmd=channel.getroot&cid=' + self.data.cid + '&auth_key=' + self.data.auth_key + '&type=' + channel_type + '&r=' + random_number(),
			crossDomain: true, 
			dataType: 'json',
			
			success:function(json){
				if (json.response) {
					if (json.response.error){
						a(json.response.error.message || json.response.error.code);
						window.history.back();
						return false;
					}
					
					//Save this data to JSON
					self.json = json.response;
					
					
					//Re-save params
					self.cid = App.hparams.cid;
					Channels.update_channel(self.cid, self.json.channel_data);
					
					//run called function
					caller_function();
				} else {
					a(error);
					window.history.back();
				}
			},
			error:function(error){
				//Show error if data not loaded
				a(error);
				window.history.back();
			}
		});
		
		return true;
	}
	
	self.load_template = function(name){
		//If loaded template just return.
		if (self.templates[name]) return false;
		var name = name;
		//Load js of template
		var script = document.createElement( "script" )
		script.type = "text/javascript";
		
		script.onload = function() {
			self.templates[name] = Gallery;
			self.channel_show();
		};
		
		script.src = App.app_url + 'templates/' + name + '/base.js?v=' + AppVer;
		
		document.getElementsByTagName( "head" )[0].appendChild( script );
		
		//Load css of template
		var css = document.createElement( "link" )
		css.type = "text/css";
		css.rel = "stylesheet";
		
		css.href = App.app_url + 'templates/' + name + '/style.css?v=' + AppVer;
		
		document.getElementsByTagName( "head" )[0].appendChild( css );
		//retrun true to check while files are will be loaded
		return true;
	}
	
	self.channel_show = function(load_channel) {
		if (load_channel === true) self.load = true;
		
		//Load channels class for get all channels list
		if ( typeof Channels == 'undefined') {
			App.append_js('index');
			return;
		}
		
		self.data = Channels.get_channel_by_cid(App.hparams.cid);
		
		if (self.load_data()) return;
		
		//Load templates files
		if (self.load_template(self.json.channel_data.ctype)) return;
		
		self.gallery = new self.templates[self.data.ctype];
		
		window.addEventListener("hashchange", Channel.hash_change, true);
		
		if (typeof self.gallery == 'object') {
			self.gallery.init(self.json);
		}
	}
	
	self.hash_change = function() {
		if (App.hparams.page == 'channel-show') {
			self.gallery.subpage = App.hparams.subpage;
			
			self.gallery.change_subpage();
		} else {
			$(self.gallery).empty();
			window.removeEventListener("hashchange", Channel.hash_change, true);
		}
	}
}
var Channel = new ChannelClass();

