var saved_channels = [];

var ChannelClass = function() {
	var self 			= this;
	self.saved_channels = [];
	self.page 			= false;
	
	self.init = function(){
		self.get_channels_list();
		
		if (self.saved_channels.length == 0) {
			window.location.hash = '#' + $.param({page : 'add-channel'});
			return;
		}
	}
	/**
	 *Page: channel-list 
	 */
	self.print_channels_list = function() {
		//Create page
		self.page = App.print_page_block('channels_list', false, 'Channels');
		//Print add button
		var add_chanel = $('<a>', {
			id : 'add_channel',
			href : '#' + $.param({page : 'add-channel'})
		}).html('+');
		 $('#header').append(add_chanel);
		
		//Append list of channels
		var channels = $('<div>', {
			class : 'channels'
		}).appendTo(self.page);
		
		for (var n in self.saved_channels) {
			var params = {	page: 'channel-show',
							cid: self.saved_channels[n].cid};
			var channel = $('<a>', {
				class : 'channel',
				href: '#' + $.param(params),
				'data-id' : self.saved_channels[n].cid
			}).appendTo(channels);
			
			var logo_div = $('<div>', {class: 'logo_div'}).appendTo(channel);
			var logo_image = $('<div>', {class: 'logo_image'}).appendTo(logo_div);
			if (self.saved_channels[n].metajson 
				&& self.saved_channels[n].metajson.logo) {
				logo_image.css('background-image', self.saved_channels[n].metajson.logo);
			}
			
			var ul = $('<ul>', {
				class : 'added_channel_information'
			}).appendTo(channel);
			
			
			$('<li>', {class: 'channel_link'}).appendTo(ul).html(self.saved_channels[n].cname).click(
				function(event) {
					$(this).addClass('loading');
				}
			);
			
			$('<li>').appendTo(ul).html(self.saved_channels[n].cdesc);
			
			$('<li>', {class: 'channel_price'}).appendTo(ul)
				.append($('<span>', {class: 'fa fa-circle ' + self.saved_channels[n].cprice}))
				.append($('<span>').html((self.saved_channels[n].cprice == '' ? 'Free' : self.saved_channels[n].cprice))); 
			
			//Add remove channel button
			$('<a>', {class : 'btn btn-remove remove_channel', href : 'javascript:void(0);', 'data-channel_id' : self.saved_channels[n].cid}).html('<span class="fa fa-minus-square"></span>').appendTo(channel)
				.click(function(event) {
					var cid = $(this).attr('data-channel_id');
					for (var k in self.saved_channels) {
						if (self.saved_channels[k].cid == cid) {
							if (confirm("Are you realy wont delete this channel?")){
								self.saved_channels.splice(k, 1);
								self.save_channels_list();
								
								if (self.saved_channels.length > 0) {
									$(this).parent().remove();
								}
								else {
									window.location.hash = '#' + $.param({page : 'add-channel'});
								}
							}
							return;
						}
					}
				event.stopPropagation();
				event.preventDefault();
				return;
				});
		}
	}
	
	/**
	 *Page: add-channel 
	 */
	self.print_add_channel = function() {
	
		var params  = {page: 'page-channels_list'};
		var back_button = $('<a>', {class:'fa fa-arrow-left', id: 'menu_button'})
							.attr('href', '#' + $.param(params));
		
		//Create page
		self.page = App.print_page_block('add_channel_block', false, 'Add channel', back_button);
		
		
		$('<h3>').appendTo(self.page).html('Type pair code and click ok');
		var non_number = String.fromCharCode('0x25CF');
		
		var code = $('<input>', {
			id : 'channel_code',
			type : 'number',
			placeholder : 'Pair code',
			maxlength: 6
		}).appendTo(self.page);
		
		var numbers = $('<div>', {class: 'numbers'}).appendTo(self.page).click(function(){
			$(code).focus();
		});
		for (var i = 1; i <= 6; i++){
			$('<span>', {class: 'number'}).html(non_number).appendTo(numbers);
		}
		
		code.on('keyup', function(){
			var number = $(this).val();
			
			for (var i = 0; i < 6; i++){
				var inum = number[i] || non_number;
				$(numbers).find('.number').eq(i).html(inum);
			}
		});
		$(code).focus();
		
		var buttons = $('<div>', {class:'buttons'}).appendTo(self.page);
		$('<a>', {
			class : 'btn btn-cancel',
			href : '#' + $.param({page : 'channels-list'}),
			html : 'Cancel'
		}).appendTo(buttons);
		
		$('<a>', {
			id : 'add_code',
			href : 'javascript:void(0)',
			html : 'Ok',
			class: 'btn btn-primary'
			})
		.appendTo(buttons)
		.click(function(event) {
			
			var code = $('#channel_code').val().replace('/[^\d]+/', '');
			
			if (code.length > 0) {
				
				if (code == '0933'){
					if (confirm('Enter in developmet mode ?')) {
						window.localStorage.setItem('testmode', 'development');
						if (App.parser.os.name == 'android') 
							alert('Development server ON');
						else 
							alert("Development server ON\r\nPlease close the app.")
					} else {
						window.localStorage.setItem('testmode', '');
						if (App.parser.os.name == 'android') 
							alert('Development server OFF');
						else 
							alert("Development server OFF\r\nPlease close the app.")
					}
					navigator.app.exitApp();
				} else {
					$('#page-add_channel_block').addClass('loading');
					$.ajax({
						url : App.ajax_url + '?cmd=pair.auth&key=' + code + '&r=' + random_number(),
						crossDomain : true,
						dataType : 'json',
						beforeSend : function() {
							$('#channel_code').addClass('ajax_request');
						},
						complete : function() {
							$('#page-add_channel_block').removeClass('loading');
							$('#channel_code').removeClass('ajax_request');
						},
		
						success : function(json) {
							
							if (json && json.response) {
								
								if (json.response.result == 'ok') {
									self.saved_channels.push(json.response.channel);
									self.save_channels_list();
									self.print_added_chanel(json.response.channel);
								} else if (json.response.error) {
									if (json.response.error.message != '') {
										a(json.response.error.message);
									} else {
										a(json.response.error.code);
									}
								}
		
							}
							else {
								a(json);
							}
						},
						error : a
					});
				}
				
			}
			else {
				if (code == '') {
					alert('Your code is empty');
				}
				else {
					alert('You code are wrong!!');
				}
			}
		});
		
	}
	
	self.print_added_chanel  = function(channel){
		$(self.page).empty();
		var div = $('<div>', {id : 'page-added_channel_block'}).appendTo(self.page);
	
		$('<h3>').appendTo(div).html('Information about added chanel:');
		
		var params = {	page: 'channel-show',
						cid: channel.cid};
		var channel_div = $('<a>', {class: 'channel', 'href': '#' + $.param(params)}).appendTo(div);
		
		var logo_div = $('<div>', {class: 'logo_div'}).appendTo(channel_div);
		var logo_image = $('<div>', {class: 'logo_image'}).appendTo(logo_div);
		if (channel.metajson 
				&& channel.metajson.logo) {
				logo_image.css('background-image', self.saved_channels[n].metajson.logo);
			}
		$('<h2>').html(channel.cname).appendTo(channel_div);
		$('<p>').html(channel.cdesc).appendTo(channel_div);
		
		$('<a>', {
			class : 'btn btn-success',
			href : '#' + $.param({page : 'channels-list'})
		}).html('Ok').appendTo(div);
	}
	
	/**
	 *Some small functions 
	 */
	
	self.update_channel = function(cid, channel){
		var old_channel = self.get_channel_by_cid(cid);
		
		for (var name in old_channel){
			old_channel[name] = channel[name] || old_channel[name];
		}
		
		for(var i in self.saved_channels){
			if (self.saved_channels[i].cid == old_channel.cid){
				self.saved_channels[i].cid = old_channel.cid;
				self.save_channels_list();
				return old_channel;
			}
		}
		
		return false;
	}
	
	self.get_channels_list = function(){
		
		if (window.localStorage.getItem('saved_channels') && window.localStorage.getItem('saved_channels') != '') {
			self.saved_channels = JSON.parse(window.localStorage.getItem('saved_channels'));
		}
		return self.saved_channels;
	}
	
	self.save_channels_list = function(){
		window.localStorage.setItem('saved_channels', JSON.stringify(self.saved_channels));
		return self.saved_channels;
	}
	
	self.get_channel_by_cid = function(cid) {
		self.get_channels_list();
		
		for (var i in self.saved_channels){
			if (self.saved_channels[i].cid == cid) 
				return self.saved_channels[i];
		}
		
		return false;
	}
	
	self.init();
}
var Channels = new ChannelClass();